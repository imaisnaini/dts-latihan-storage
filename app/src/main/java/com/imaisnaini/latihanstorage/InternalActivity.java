package com.imaisnaini.latihanstorage;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InternalActivity extends AppCompatActivity {
    @BindView(R.id.tvReadFile)
    TextView tvReadFile;

    public static final String FILENAME = "namafile.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.btnAddFile) public void addFile(){
        String isiFile = "Ini data dummy untuk text file.";
        File file = new File(getFilesDir(), FILENAME);

        FileOutputStream  outputStream = null;
        try {
            file.createNewFile();
            outputStream = new FileOutputStream(file, true);
            outputStream.write(isiFile.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnEditFile) public void editFile(){
        String isiFile = "Ini text dummy yang sudah di edit.";
        File file = new File(getFilesDir(), FILENAME);

        FileOutputStream  outputStream = null;
        try {
            file.createNewFile();
            outputStream = new FileOutputStream(file, true);
            outputStream.write(isiFile.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnReadFile) public void readFile(){
        File sdcard = getFilesDir();
        File file = new File(sdcard, FILENAME);

        if (file.exists()){
            StringBuilder text = new StringBuilder();
            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line =  br.readLine();

                while(line != null){
                    text.append(line);
                    line = br.readLine();
                }

                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            tvReadFile.setText(text.toString());
        }
    }

    @OnClick(R.id.btnDeleteFile) public void deleteFile(){
        File file = new File(getFilesDir(), FILENAME);
        if (file.exists()){
            file.delete();
        }
        tvReadFile.setText("");
    }
}
